Name:           gnome-autoar
Version:        0.4.4
Release:        1
Summary:        Creating and extracting archives
License:        LGPLv2+
URL:            https://gitlab.gnome.org/GNOME/gnome-autoar
Source0:        https://download.gnome.org/sources/%{name}/0.4/%{name}-%{version}.tar.xz

BuildRequires:  gcc vala pkgconfig(gio-2.0) pkgconfig(glib-2.0) pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gobject-introspection-1.0) pkgconfig(gtk+-3.0) pkgconfig(libarchive)
BuildRequires:  meson gtk-doc

%description
Automatic archives creating and extracting library.

%package devel
Summary:   development header files, libraries for application use the gnome-autoar package
Requires:  %{name} = %{version}-%{release}

%description devel
development header files, libraries for programs using the gnome-autoar library.

%prep
%autosetup -p1

%build
%meson -Dvapi=true \
       -Dgtk_doc=true \
       -Dtests=true \
       %{nil}

%meson_build

%install
%meson_install
%delete_la

%check
%meson_test

%files
%doc NEWS
%license COPYING
%{_libdir}/girepository-1.0/*.typelib
%{_libdir}/libgnome-autoar-0.so.0*
%{_libdir}/libgnome-autoar-gtk-0.so.0*

%files devel
%{_includedir}/gnome-autoar-0/
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_datadir}/gir-1.0/*.gir
%{_datadir}/gtk-doc/
%{_datadir}/vala/vapi/*.vapi
%{_datadir}/vala/vapi/gnome-autoar-0.deps
%{_datadir}/vala/vapi/gnome-autoar-gtk-0.deps

%changelog
* Wed Nov 22 2023 lwg <liweiganga@uniontech.com> - 0.4.4-1
- update to version 0.4.4

* Wed May 18 2022 lwg <liweiganga@uniontech.com> - 0.4.3-2
- fix spec changelog date

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.4.3-1
- Upgrade to 0.4.3

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 0.2.4-4
- delete gdb in buildrequires

* Wed Apr 14 2021 Dehui Fan <fandehui1@huawei.com> - 0.2.4-3
- Type: CVE
- ID:   CVE-2021-28650 
- SUG:  NA
- DESC: fix CVE-2021-28650, remove CVE-2020-36241 

* Fri Mar 19 2021 Dehui Fan <fandehui1@huawei.com> - 0.2.4-2
Type: CVE
ID:   CVE-2020-36241
SUG:  NA
DESC: fix CVE-2020-36241

* Fri Jan 29 2021 yanglu <yanglu60@huawei.com> - 0.2.4-1
- update to 0.2.4

* Fri Mar 20 2020 songnnannan <songnannan2@huawei.com> - 0.2.3-4
- add gdb in buildrequires

* Tue Sep 17 2019 Lijin Yang <yanglijin@huawei.com> - 0.2.3-3
- Package init
